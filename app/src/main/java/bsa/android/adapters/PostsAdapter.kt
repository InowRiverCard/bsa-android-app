package bsa.android.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import bsa.android.R
import bsa.android.data.Post
import bsa.android.databinding.RowPostHomeBinding

const val POST_ID = "POST_ID"
const val AUTHOR_ID = "USER_ID"

class PostsAdapter(private val postList: List<Post>) :
    RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder(
            RowPostHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount() = postList.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    class PostViewHolder(rowBinding: RowPostHomeBinding) :
        RecyclerView.ViewHolder(rowBinding.root) {
        private val binding = rowBinding

        fun bind(post: Post) {
            binding.title = post.title
            binding.number = '#' + post.id.toString()
            val bundle = bundleOf(
                Pair(AUTHOR_ID, post.userId),
                Pair(POST_ID, post.id)
            )
            binding.root.setOnClickListener { view ->
                Navigation.findNavController(view).navigate(R.id.action_home_to_details, bundle)
            }
        }
    }
}