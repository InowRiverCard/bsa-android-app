package bsa.android


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import bsa.android.databinding.FragmentHomeBinding
import bsa.android.databinding.FragmentHomeBindingImpl
import bsa.android.adapters.PostsAdapter
import bsa.android.data.Post
import bsa.android.services.ApiFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception


class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBindingImpl.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBindings()
    }

    private fun initBindings() {
        val service = ApiFactory.placeholderApi
        GlobalScope.launch(Dispatchers.Main) {
            val postRequest = service.getPosts()
            try {
                val response = postRequest.await()
                if(response.isSuccessful){
                    var posts: List<Post>? = response.body()
                    if (posts == null) {
                        posts = emptyList();
                    }
                    binding.homePostsList.apply {
                        adapter = PostsAdapter(posts)
                    }

                }else{
                    Log.d("MainActivity ",response.errorBody().toString())
                }
            }catch (e: Exception){

            }
        }

        val linearLayoutManager = LinearLayoutManager(activity)
        binding.homePostsList.apply {
            layoutManager = linearLayoutManager
        }
    }

}

