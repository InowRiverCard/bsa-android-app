package bsa.android.workers


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import bsa.android.adapters.AUTHOR_ID
import bsa.android.adapters.CommentsAdapter
import bsa.android.adapters.POST_ID
import bsa.android.adapters.PostsAdapter
import bsa.android.data.Comment
import bsa.android.data.Post
import bsa.android.data.User
import bsa.android.databinding.FragmentPostDetailsBinding
import bsa.android.databinding.FragmentPostDetailsBindingImpl
import bsa.android.services.ApiFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception


class PostDetailsFragment : Fragment() {

    private lateinit var binding: FragmentPostDetailsBinding
    private lateinit var postId: String
    private lateinit var userId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadArguments()
    }

    private fun loadArguments() {
        postId = arguments?.getInt(POST_ID).toString()
        userId = arguments?.getInt(AUTHOR_ID).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPostDetailsBindingImpl.inflate(inflater)
        initBind()
        return binding.root
    }

    private fun initBind() {
        GlobalScope.launch(Dispatchers.Main) {
            val service = ApiFactory.placeholderApi
            val userRequest = service.getUserById(userId)
            val postRequest = service.getPostById(postId)
            try {
                val postResponse = postRequest.await()
                val response = userRequest.await()
                if(response.isSuccessful){
                    val user: User? = response.body()
                    binding.postAuthorName = user?.name
                }else{
                    Log.d("MainActivity ",response.errorBody().toString())
                }
                if (postResponse.isSuccessful) {
                    val post: Post? = postResponse.body()
                    binding.body = post?.body
                    binding.title = post?.title
                }
            }catch (e: Exception){

            }
            val commentsRequest = service.getCommentsByPostId(postId)
            try {
                val response = commentsRequest.await()
                if(response.isSuccessful){
                    var comments: List<Comment>? = response.body()
                    if (comments == null) {
                        comments = emptyList()
                    }
                    val linearLayoutManager = LinearLayoutManager(activity)
                    binding.postCommentsList.apply {
                        adapter = CommentsAdapter(comments)
                        layoutManager = linearLayoutManager
                    }
                }else{
                    Log.d("Load comments ",response.errorBody().toString())
                }
            }catch (e: Exception){

            }
        }
    }
}
