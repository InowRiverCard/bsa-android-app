package bsa.android


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bsa.android.databinding.FragmentSavedContBinding
import bsa.android.databinding.FragmentSavedContBindingImpl

class SavedContainerFragment : Fragment() {

    private lateinit var binding: FragmentSavedContBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSavedContBindingImpl.inflate(inflater)
        return binding.root
    }
}
