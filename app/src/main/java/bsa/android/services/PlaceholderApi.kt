package bsa.android.services

import bsa.android.data.Comment
import bsa.android.data.Post
import bsa.android.data.User
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PlaceholderApi{

    @GET("/posts")
    fun getPosts() : Deferred<Response<List<Post>>>

    @GET("/users")
    fun getUsers() : Deferred<Response<List<User>>>

    @GET("/users/{id}")
    fun getUserById(@Path("id") id: String) : Deferred<Response<User>>

    @GET("/posts/{id}")
    fun getPostById(@Path("id") id: String) : Deferred<Response<Post>>

    @GET("/posts/{id}/comments")
    fun getCommentsByPostId(@Path("id") id: String) : Deferred<Response<List<Comment>>>
}