package bsa.android.services


object ApiFactory{

    const val JSON_PLACEHOLDER_BASE_URL = "https://jsonplaceholder.typicode.com"

    val placeholderApi : PlaceholderApi = RetrofitFactory.retrofit(JSON_PLACEHOLDER_BASE_URL)
                                                .create(PlaceholderApi::class.java)

}