package bsa.android.data

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.io.IOException


suspend fun getPosts(): List<Post> {
        val url = "https://jsonplaceholder.typicode.com/posts"
        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        val resp = client.newCall(request).execute()
        val gson = GsonBuilder().create()
        var result = listOf<Post>()
        if (resp.isSuccessful) {
            val body = resp.body?.string()
            result = gson.fromJson(body, Array<Post>::class.java).toList()
        } else {
            print("fail to fetch")
        }

        return result;
    }

object Apifactory{

//    //Creating Auth Interceptor to add api_key query in front of all the requests.
//    private val authInterceptor = Interceptor {chain->
//        val newUrl = chain.request().url
//            .newBuilder()
//            .build()
//
//        val newRequest = chain.request()
//            .newBuilder()
//            .url(newUrl)
//            .build()
//
//        chain.proceed(newRequest)
//    }

    //OkhttpClient for building http request url
    private val tmdbClient = OkHttpClient().newBuilder().build()



    fun retrofit() : Retrofit = Retrofit.Builder()
        .client(tmdbClient)
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()


    val posts : List<Post> = retrofit().create(Array<Post>::class.java).toList()

}